package banco;

import java.util.ArrayList;

public class GerenciarConta {
	
	static int qtdContas = 0;
	static ArrayList<Conta> contas = new ArrayList<Conta>();
	
	
	public static void addConta(Conta c1) {
		qtdContas ++;
		contas.add(c1);	
	}
	
	public static Conta consultarContaPorNome(String nome) {
		for (int i = 0; i < qtdContas; i++) {
			if (nome.equalsIgnoreCase(contas.get(i).getTitular().getNome())) {
				return contas.get(i);
			}
		}
		return null;
	}

}
