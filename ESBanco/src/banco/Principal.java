package banco;

import java.util.ArrayList;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in); 
		int menu = 0; 
		String nome;
		String CPF;
		int idade;
		int op=0; 

		do {
			System.out.println("'''''''' MENU ''''''''");
			System.out.println("1- ADICIONAR PESSOA");
			System.out.println("2- CONSULTAR PESSOA ");
			System.out.println("3- ATIVAR CONTA ");
			System.out.println("4- DESATIVAR CONTA ");
			System.out.println("5- SAIR ");
			menu = Integer.valueOf(entrada.nextLine()); 



			switch(menu) {

			case 1:

				System.out.println("'''' ADICIONAR PESSOA ''''");
				System.out.println("Insira o nome:");
				nome = entrada.nextLine();
				System.out.print("Insira o CPF:");
				CPF = entrada.nextLine(); 
				System.out.print("Insira a idade");
				idade = Integer.valueOf(entrada.nextLine()); 
				Pessoa p1 = new Pessoa(nome,CPF, idade); 
				GerenciarPessoa.addPessoa(p1);
				System.out.println("Pessoa adicionada ao sistema.");
				break;
			case 2:
				System.out.println("'''' CONSULTAR PESSOA ''''");
				System.out.println("Digite o nome da pessoa que deseja consultar:");
				nome = entrada.nextLine(); 
				GerenciarPessoa.consultaPessoa(nome);
				break; 
			case 3:
				System.out.println("'''' DESATIVAR CONTA ''''");
				System.out.println("Digite o nome da pessoa que deseja consultar:");
				nome = entrada.nextLine();
				Conta consultada = GerenciarConta.consultarContaPorNome(nome);
				if (consultada == null) {
					System.out.println("Conta n�o encontrada!");
				} else {
					consultada.desativaConta();
					System.out.println("Conta Desativada com Sucesso!");
				}
				break;
			case 4:
				System.out.println("'''' ATIVAR CONTA ''''");
				System.out.println("Digite o nome da pessoa que deseja consultar:");
				nome = entrada.nextLine();
				Conta consultada2 = GerenciarConta.consultarContaPorNome(nome);
				if (consultada2 == null) {
					System.out.println("Conta n�o encontrada!");
				} else {
					consultada2.ativaConta();
					System.out.println("Conta Ativada com Sucesso!");
				}
				break;
			case 5:
				System.out.println("'''VOC� SAIU DO PROGRAMA! '''");


			}


		}while(menu!=3);


	}




}
