package banco;

import java.util.ArrayList;

public class Pessoa {
	private int qtdDependentes;
	private String nome;
	private String cpf;
	private int idade;
	private ArrayList<Dependente> dependentes = new ArrayList<Dependente>();
	
	public Pessoa(String nome, String cpf, int idade) {
		this.nome = nome;
		this.cpf = cpf;
		this.idade = idade;
		
	}


	public int getQtdDependentes() {
		return qtdDependentes;
	}

	public void setQtdDependentes(int qtdDependentes) {
		this.qtdDependentes = qtdDependentes;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public void addDependente(Dependente d) {
		dependentes.add(d); 
	}

	public void consultaDependente(String nome) {
		for (int i = 0; i < dependentes.size(); i++) {
			if(nome.equalsIgnoreCase(dependentes.get(i).getNome())) {
			System.out.println(dependentes.get(i).toString()); 
			}
		}
	}
	@Override
	public String toString() {
		return "Pessoa [qtdDependentes=" + qtdDependentes + ", nome=" + nome + ", cpf=" + cpf + ", idade=" + idade
				+ "]";
	}

}

