package banco;



import java.util.Scanner;

public class Conta {

	private Pessoa titular;
	private int num;
	private double saldo;
	private double limite;
	private boolean isAtivo;

	public Conta(Pessoa titular, int num, double saldo, double limite, boolean isAtivo) {
		this.titular = titular;
		this.num = num;
		this.saldo = saldo;
		this.limite = limite;
		this.isAtivo = isAtivo;
	}

	public Pessoa getTitular() {
		return titular;
	}
	public void setTitular(Pessoa titular) {
		this.titular = titular;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public double getLimite() {
		return limite;
	}
	public void setLimite(double limite) {
		this.limite = limite;
	}
	public boolean isAtivo() {
		return isAtivo;
	}
	public void setAtivo(boolean isAtivo) {
		this.isAtivo = isAtivo;
	}

	public void sacar(Double saque) {

		if (saque <= (this.saldo + this.limite)) {
			saldo -= saque;
			System.out.println("Saque Realizado com Sucesso!");
		} else {
			System.out.println("Saldo Insuficiente!");
		}
	}

	public void depositar(double valor) {
		if (valor > 0) {
			this.saldo += valor;
		} else {
			System.out.println("N�o � possivel realizar o deposito");
		}

	}

	public void extrato() {
		System.out.println("SALDO:" + this.saldo);


	}

	public void desativaConta() {

		this.setAtivo(false);

	}

	public void ativaConta() {

		this.setAtivo(true);
	}



}


